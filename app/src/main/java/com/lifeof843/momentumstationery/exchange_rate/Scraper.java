package com.lifeof843.momentumstationery.exchange_rate;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class Scraper extends AsyncTask<Void[], Void, Void> {

    private static final String link = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    private ExchangeRateProcessor exchangeRateProcessor;

    Scraper(ExchangeRateProcessor exchangeRateProcessor) {
        this.exchangeRateProcessor = exchangeRateProcessor;
    }

    @Override
    protected Void doInBackground(Void[]... params) {
        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            try {
                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                readStream(inputStream);
            } finally {
                connection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void readStream(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder content = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            content.append(line);
        }

        exchangeRateProcessor.sendResult(content.toString());
    }
}
