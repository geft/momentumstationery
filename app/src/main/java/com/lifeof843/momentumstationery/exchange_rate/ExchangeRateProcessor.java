package com.lifeof843.momentumstationery.exchange_rate;

import android.content.Context;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExchangeRateProcessor {

    private static final String regexUSD = "\'USD\' rate=\'(.*?)\'";
    private static final String regexIDR = "\'IDR\' rate=\'(.*?)\'";
    private static final String regexTime = "time=\'(.*?)\'";

    private IScraper listener;

    public ExchangeRateProcessor(Context context) {
        new Scraper(this).execute();

        try {
            this.listener = (IScraper) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    private ExchangeRateData parseContent(String content) {
        float USD = getMatchedCurrency(content, regexUSD);
        float IDR = getMatchedCurrency(content, regexIDR);
        String time = getTime(content, regexTime);

        return new ExchangeRateData((int) getCalculatedRate(USD, IDR), time);
    }

    private String getTime(String content, String regexTime) {
        Pattern pattern = Pattern.compile(regexTime);
        Matcher matcher = pattern.matcher(content);

        String time = null;
        if (matcher.find()) {
            time = matcher.group(1);
        }

        return time;
    }

    private float getCalculatedRate(float USD, float IDR) {
        return (USD == 0) ? 0 : IDR / USD;
    }

    private float getMatchedCurrency(String content, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);

        String value = "0";
        if (matcher.find()) {
            value = matcher.group(1);
        }

        return Float.parseFloat(value);
    }

    public void sendResult(String content) {
        ExchangeRateData rateData = parseContent(content);
        listener.onScrapeDone(rateData.rate, rateData.time);
    }

    private class ExchangeRateData {
        int rate;
        String time;

        private ExchangeRateData(int rate, String time) {
            this.rate = rate;
            this.time = time;
        }
    }
}
