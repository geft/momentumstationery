package com.lifeof843.momentumstationery.utility;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.activity.About;
import com.lifeof843.momentumstationery.activity.PriceList;
import com.lifeof843.momentumstationery.activity.VendorList;
import com.lifeof843.momentumstationery.dropbox.DropboxLoader;

import java.util.ArrayList;
import java.util.List;

public class MenuItemBuilder {
    public static final int ANIMATION_SPEED = 1000;
    private Context context;
    private DropboxLoader dropboxLoader;

    public MenuItemBuilder(Context context, DropboxLoader dropboxLoader) {
        this.context = context;
        this.dropboxLoader = dropboxLoader;

        init();
    }

    private void init() {
        initLoadDatabase();
        initPriceList();
        initVendorList();
        initAbout();
    }

    private View getViewFromId(int res) {
        return ((Activity) context).findViewById(res);
    }

    private ImageButton getImageButtonFromId(int res) {
        return (ImageButton) ((Activity) context).findViewById(res);
    }

    private void initLoadDatabase() {
        final ImageButton button = getImageButtonFromId(R.id.imageLoadDatabase);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateAllViewsExit(ButtonType.LOAD_DATABASE);
            }
        });
    }

    private void initPriceList() {
        final ImageButton button = getImageButtonFromId(R.id.imagePriceList);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateAllViewsExit(ButtonType.PRICE_LIST);
            }
        });
    }

    private void initVendorList() {
        final ImageButton button = getImageButtonFromId(R.id.imageVendorList);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateAllViewsExit(ButtonType.VENDOR_LIST);
            }
        });
    }

    private void initAbout() {
        final ImageButton button = getImageButtonFromId(R.id.imageAbout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateAllViewsExit(ButtonType.ABOUT);
            }
        });
    }

    private List<View> getAnimatedViews() {
        List<View> viewList = new ArrayList<>();
        viewList.add(getViewFromId(R.id.imageLoadDatabase));
        viewList.add(getViewFromId(R.id.priceListContainer));
        viewList.add(getViewFromId(R.id.vendorListContainer));
        viewList.add(getViewFromId(R.id.imageAbout));

        return viewList;
    }

    public void animateAllViewsEnter() {
        List<View> viewList = getAnimatedViews();
        int animSpeed = ANIMATION_SPEED / viewList.size();
        for (int i = 0; i < viewList.size(); i++) {
            View view = viewList.get(i);
            view.animate()
                    .setStartDelay((viewList.size() - i) * animSpeed)
                    .alpha(1.0f)
                    .setListener(null);
        }
    }

    private void animateAllViewsExit(ButtonType buttonType) {
        List<View> viewList = getAnimatedViews();
        int animSpeed = ANIMATION_SPEED / viewList.size();
        for (int i = 0; i < viewList.size(); i++) {
            viewList.get(i).animate()
                    .setStartDelay(i * animSpeed)
                    .alpha(0.0f)
                    .setListener(getAnimatorListener(i == viewList.size() - 1, buttonType));
        }
    }

    private Animator.AnimatorListener getAnimatorListener(final boolean isLast, final ButtonType buttonType) {
        return new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isLast) {
                    performAction(buttonType);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        };
    }

    private void performAction(ButtonType buttonType) {
        switch (buttonType) {
            case LOAD_DATABASE:
                dropboxLoader.createLoadDatabaseDialog(context);
                break;
            case PRICE_LIST:
                context.startActivity(new Intent(context, PriceList.class));
                break;
            case VENDOR_LIST:
                context.startActivity(new Intent(context, VendorList.class));
                break;
            case ABOUT:
                context.startActivity(new Intent(context, About.class));
                break;
        }

        Systems.fadeAnimation((Activity) context);
    }

    private enum ButtonType {
        LOAD_DATABASE,
        PRICE_LIST,
        VENDOR_LIST,
        ABOUT
    }
}
