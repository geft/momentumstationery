package com.lifeof843.momentumstationery.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.EditText;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.fragment.LoginAdmin;

public class PasswordManager {
    public void createPasswordChangeDialog(Context context) {
        final View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.settings_change_password, null);
        final AlertDialog dialog = Dialogs.getTwoWayDialog(
                view, context.getString(R.string.change_password_title));
        dialog.show();
        setChangePasswordPositive(view, dialog);
    }

    private void setChangePasswordPositive(final View view, final AlertDialog dialog) {
        dialog.getButton(
                DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (changePassword(view)) {
                    dialog.dismiss();
                }
            }
        });
    }

    private boolean changePassword(View view) {
        Context context = view.getContext();
        String editCurrent = getEditText(view, R.id.editCurrent);
        String editNew = getEditText(view, R.id.editNew);
        String editNew2 = getEditText(view, R.id.editNew2);

        boolean result = false;
        if (!editCurrent.equals(Prefs.readPassword(context))) {
            Systems.makeToast(context, context.getString(R.string.change_password_current_mismatch));
        } else if (!editNew.equals(editNew2)) {
            Systems.makeToast(context, context.getString(R.string.change_password_new_mismatch));
        } else {
            Prefs.writePassword(context, editNew);
            Systems.makeToast(context, context.getString(R.string.change_password_success));
            result = true;
        }
        return result;
    }

    private String getEditText(View view, int resId) {
        EditText editText = (EditText) view.findViewById(resId);
        return editText.getText().toString();
    }

    public void createAdminPromptDialog(FragmentManager fragmentManager) {
        DialogFragment fragment = LoginAdmin.newInstance();
        fragment.show(fragmentManager, "dialog");
    }
}
