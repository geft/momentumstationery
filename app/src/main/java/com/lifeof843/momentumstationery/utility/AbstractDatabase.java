package com.lifeof843.momentumstationery.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.lifeof843.momentumstationery.dropbox.GenericParser;

import java.util.ArrayList;

public abstract class AbstractDatabase extends SQLiteOpenHelper {

    public static String COL_ID = "_id";
    public static String COL_NAME = "name";

    protected String TABLE_NAME = getDbTableName();
    private String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME;
    protected SQLiteDatabase database;
    private String SQL_CREATE_ENTRIES = getDbQueryCreate();

    public AbstractDatabase(Context context, String databaseName) {
        super(context, databaseName, null, 1);

        initDatabase();
    }

    protected abstract String getDbTableName();

    protected abstract String getDbQueryCreate();

    private void initDatabase() {
        database = getWritableDatabase();
    }

    public boolean doesDatabaseExist() {
        boolean doesExist = false;

        Cursor cursor = getCursor();
        if (cursor.moveToFirst()) {
            doesExist = true;
        }
        cursor.close();

        return doesExist;
    }

    public boolean doesEntryExist(String name) {
        boolean doesExist = false;

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME +
                " WHERE " + COL_NAME + "=?", new String[]{name});
        if (cursor.moveToFirst()) {
            doesExist = true;
        }
        cursor.close();

        return doesExist;
    }

    public Cursor getCursor() {
        return database.rawQuery(
                "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COL_NAME + " ASC", null);
    }

    protected String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    protected void insertEntry(
            ArrayList dataEntries, GenericParser.DownloadUpdater updater, int exchangeRate) {
        ContentValues contentValues = new ContentValues();
        int count = 0;
        for (Object data : dataEntries) {
            addEntry(contentValues, exchangeRate, data);
            updater.updateProgress(count);
            count++;
        }
    }

    public abstract void addEntry(ContentValues contentValues, int exchangeRate, Object data);

    public void deleteEntry(String name) {
        database.delete(TABLE_NAME, COL_NAME + "=?", new String[]{name});
    }

    protected void resetDatabase() {
        onUpgrade(database, 1, 1);
    }

    public boolean isLocked() {
        return database.isOpen() && database.isDbLockedByCurrentThread();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
