package com.lifeof843.momentumstationery.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.lifeof843.momentumstationery.R;

public class Dialogs {
    public static AlertDialog getTwoWayDialog(View view, String title) {
        Context context = view.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setTitle(title);
        builder.setPositiveButton(context.getString(R.string.button_OK), null);
        builder.setNegativeButton(context.getString(R.string.button_cancel), null);
        return builder.create();
    }

    public static AlertDialog getTwoWayDialog(View view, String title,
                                              DialogInterface.OnClickListener listener) {
        Context context = view.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setTitle(title);
        builder.setPositiveButton(context.getString(R.string.button_OK), listener);
        builder.setNegativeButton(context.getString(R.string.button_cancel), null);
        return builder.create();
    }

    public static AlertDialog getWarningPrompt(Context context, String text,
                                               DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.drawable.ic_ic_warning_24px);
        builder.setTitle(context.getString(R.string.dialog_prompt));
        builder.setMessage(text);
        builder.setPositiveButton(context.getString(R.string.button_OK), listener);
        builder.setNegativeButton(context.getString(R.string.button_cancel), null);
        return builder.create();
    }
}
