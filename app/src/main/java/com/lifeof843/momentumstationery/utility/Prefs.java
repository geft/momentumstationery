package com.lifeof843.momentumstationery.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {

    public static final String FILE_NAME = "data";
    public static final String PASSWORD = "password";
    public static final String DEFAULT_PASSWORD = "hikari";
    public static final String DROPBOX_KEY = "dropbox";
    public static final String EXCHANGE_RATE = "rate";
    public static final int DEFAULT_EXCHANGE_RATE = 13200;
    public static final String EXCHANGE_RATE_TIME = "time";
    public static final String DEFAULT_EXCHANGE_RATE_TIME = "Unknown";

    private static SharedPreferences getPrivateSharedPreference(Context context) {
        return context.getSharedPreferences(
                Prefs.FILE_NAME, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getSharedPreferenceEditor(Context context) {
        return getPrivateSharedPreference(context).edit();
    }

    public static int readExchangeRate(Context context) {
        SharedPreferences preferences = getPrivateSharedPreference(context);
        return preferences.getInt(EXCHANGE_RATE, DEFAULT_EXCHANGE_RATE);
    }

    public static void writeExchangeRate(Context context, int exchangeRate) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putInt(EXCHANGE_RATE, exchangeRate);
        editor.apply();
    }

    public static String readExchangeRateTime(Context context) {
        SharedPreferences preferences = getPrivateSharedPreference(context);
        return preferences.getString(EXCHANGE_RATE_TIME, DEFAULT_EXCHANGE_RATE_TIME);
    }

    public static void writeExchangeRateTime(Context context, String time) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putString(EXCHANGE_RATE_TIME, time);
        editor.apply();
    }

    public static String readPassword(Context context) {
        SharedPreferences sharedPreferences = getPrivateSharedPreference(context);
        return sharedPreferences.getString(
                PASSWORD, DEFAULT_PASSWORD);
    }

    public static void writePassword(Context context, String newPassword) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putString(PASSWORD, newPassword);
        editor.apply();
    }
}
