package com.lifeof843.momentumstationery.dropbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.widget.EditText;

import com.dropbox.chooser.android.DbxChooser;
import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Dialogs;
import com.lifeof843.momentumstationery.utility.Prefs;
import com.lifeof843.momentumstationery.utility.Systems;

import java.io.File;

public class DropboxLoader {

    public static final String DEFAULT_DROPBOX_KEY = "ctous95os5wb16z";
    public static final int DROPBOX_CONTENT_REQUEST = 1001;

    public String APP_KEY;
    private DbxChooser dbxChooser;

    public DropboxLoader(Context context) {
        APP_KEY = getDropBoxKey(context);
        this.dbxChooser = new DbxChooser(APP_KEY);
    }

    private String getDropBoxKey(Context context) {
        SharedPreferences sharedPreferences =
                context.getSharedPreferences(Prefs.FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(Prefs.DROPBOX_KEY, DEFAULT_DROPBOX_KEY);
    }

    public void createLoadDatabaseDialog(Context context) {
        if (dbxChooser != null) {
            dbxChooser.forResultType(DbxChooser.ResultType.FILE_CONTENT)
                    .launch((Activity) context, DROPBOX_CONTENT_REQUEST);
        }
    }

    public void handleDropBoxResult(int requestCode, int resultCode, Intent data, Context context) {
        if (requestCode == DROPBOX_CONTENT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                fulfilDropBoxRequest(data, context);
            }
        } else {
            Systems.makeToast(context, context.getString(R.string.load_database_invalid_request));
        }
    }

    private void fulfilDropBoxRequest(Intent data, Context context) {
        File file = getFileFromDropBoxResult(data);

        String fileName = file.getName().toLowerCase();
        if (!fileName.endsWith("csv")) {
            Systems.makeToast(context, context.getString(R.string.load_database_wrong_format));
        } else if (fileName.contains("price")) {
            new PriceListParser().readFile(context, file);
        } else if (fileName.contains("vendor")) {
            new VendorListParser().readFile(context, file);
        } else {
            Systems.makeToast(context, context.getString(R.string.load_database_file_name_error));
        }
    }

    private File getFileFromDropBoxResult(Intent data) {
        DbxChooser.Result result = new DbxChooser.Result(data);
        Uri uri = result.getLink();
        return new File(uri.getPath());
    }

    public void changeKey(final Context context) {
        final View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.settings_change_dropbox_key, null);
        final AlertDialog dialog = Dialogs.getTwoWayDialog(
                view, context.getString(R.string.settings_dropbox_key));
        dialog.show();
        setChangeKeyPositive(context, view, dialog);
    }

    private void setChangeKeyPositive(final Context context, final View view, final AlertDialog dialog) {
        dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String newKey = getStringFromEditText(view, R.id.editKey);
                        if (isKeyValid(newKey)) {
                            writeKeyToPreference(context, newKey);
                            dialog.dismiss();
                        } else {
                            Systems.makeToast(context,
                                    context.getString(R.string.change_key_invalid));
                        }

                    }
                });
    }

    private String getStringFromEditText(View view, int resId) {
        return ((EditText) view.findViewById(resId)).getText().toString();
    }

    private boolean isKeyValid(String key) {
        return key.length() == 15 && !key.contains(" ");
    }

    private void writeKeyToPreference(Context context, String newKey) {
        SharedPreferences.Editor editor =
                context.getSharedPreferences(Prefs.FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(Prefs.DROPBOX_KEY, newKey);
        editor.apply();
    }
}
