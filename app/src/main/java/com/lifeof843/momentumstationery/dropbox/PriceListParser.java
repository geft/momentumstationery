package com.lifeof843.momentumstationery.dropbox;

import android.content.Context;

import com.lifeof843.momentumstationery.pricelist.PriceData;
import com.lifeof843.momentumstationery.pricelist.PriceDatabase;
import com.lifeof843.momentumstationery.utility.Prefs;

import org.supercsv.cellprocessor.ConvertNullTo;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.ICsvBeanReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class PriceListParser extends GenericParser {

    PriceListProcessor processor;

    @Override
    protected void beginAsyncTask(Context context, File file) {
        processor = new PriceListProcessor(this, context, file);
        processor.execute();
    }

    @Override
    protected DownloadUpdater getProcessor() {
        return processor;
    }

    @Override
    protected void beginPopulatingSqlDatabase(Context context, ArrayList dataEntries) {
        PriceDatabase priceDatabase = new PriceDatabase(context);
        priceDatabase.populateDatabase(dataEntries, Prefs.readExchangeRate(context), processor);
    }

    @Override
    protected ArrayList parseCSV(File file) throws IOException {
        ArrayList<PriceData> dataEntries = new ArrayList<>();
        PriceData priceData;

        ICsvBeanReader beanReader = null;
        try {
            beanReader = getBeanReader(file);
            beanReader.getHeader(true);

            calculateRowCount(beanReader, PriceData.class);
            beanReader = restartBeanReader(beanReader, file);

            String[] headers = getProcessedHeaders();
            CellProcessor[] processors = getProcessedColumns();

            while ((priceData = beanReader.read(PriceData.class, headers, processors)) != null) {
                dataEntries.add(priceData);
            }
        } finally {
            closeBeanReader(beanReader);
        }

        return dataEntries;
    }

    @Override
    protected String[] getProcessedHeaders() {
        return new String[]{
                null, null, null, null, "name",
                null, null, null, null, null,
                null, null, "unit", null, "cost",
                null, null, null, null, null};
    }

    @Override
    protected CellProcessor[] getProcessedColumns() {
        return new CellProcessor[]{
                null, null, null, null, new NotNull(),
                null, null, null, null, null,
                null, null, new ConvertNullTo(""), null, new NotNull(),
                null, null, null, null, null
        };
    }

    @Override
    protected CellProcessor[] getNullProcessor() {
        return new CellProcessor[20];
    }
}
