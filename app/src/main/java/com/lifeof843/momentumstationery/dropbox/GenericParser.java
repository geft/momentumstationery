package com.lifeof843.momentumstationery.dropbox;

import android.content.Context;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Systems;

import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public abstract class GenericParser {

    public void readFile(Context context, File file) {
        if (file.isFile()) {
            beginAsyncTask(context, file);
        } else {
            Systems.makeToast(context, context.getString(R.string.load_database_read_fail));
        }
    }

    public ArrayList populateData(File file) {
        ArrayList arrayList;

        try {
            arrayList = parseCSV(file);
        } catch (IOException e) {
            e.printStackTrace();
            arrayList = new ArrayList<>();
        }
        return arrayList;
    }

    protected abstract DownloadUpdater getProcessor();

    protected void calculateRowCount(ICsvBeanReader beanReader, Class<?> entry) throws IOException {
        int rowCount = getRowCount(beanReader, entry);
        getProcessor().sendRowCount(rowCount);
    }

    protected ICsvBeanReader restartBeanReader(ICsvBeanReader beanReader, File file) throws IOException {
        closeBeanReader(beanReader);
        beanReader = getBeanReader(file);
        beanReader.getHeader(true);
        return beanReader;
    }

    protected abstract ArrayList parseCSV(File file) throws IOException;

    protected abstract void beginAsyncTask(Context context, File file);

    public void populateSqlDatabase(Context context, ArrayList dataEntries) {
        if (!dataEntries.isEmpty()) {
            beginPopulatingSqlDatabase(context, dataEntries);
        }
    }

    protected abstract void beginPopulatingSqlDatabase(Context context, ArrayList dataEntries);

    protected abstract String[] getProcessedHeaders();

    protected abstract CellProcessor[] getProcessedColumns();

    protected abstract CellProcessor[] getNullProcessor();

    protected int getRowCount(ICsvBeanReader beanReader, Class<?> entry) throws IOException {
        int rowCount = 0;
        while (beanReader.read(entry, getProcessedHeaders(), getNullProcessor()) != null) {
            rowCount++;
        }

        return rowCount;
    }

    protected CsvBeanReader getBeanReader(File file) throws FileNotFoundException {
        return new CsvBeanReader(
                new FileReader(file.getAbsolutePath()), CsvPreference.STANDARD_PREFERENCE);
    }

    protected void closeBeanReader(ICsvBeanReader beanReader) throws IOException {
        if (beanReader != null) {
            beanReader.close();
        }
    }

    public interface DownloadUpdater {
        public void sendRowCount(int rowCount);

        public void updateProgress(int progress);
    }
}
