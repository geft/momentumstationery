package com.lifeof843.momentumstationery.dropbox;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.pricelist.PriceData;
import com.lifeof843.momentumstationery.pricelist.PriceDatabase;

import java.io.File;
import java.util.ArrayList;

public class PriceListProcessor extends DropboxProcessor {

    private PriceListParser priceListParser;

    PriceListProcessor(PriceListParser priceListParser, Context context, File file) {
        super(context, file);
        this.priceListParser = priceListParser;

    }

    @Override
    protected ImageView getDisabledImage() {
        return (ImageView) ((Activity) context).findViewById(R.id.imageDisabled);
    }

    @Override
    protected ImageButton getLockableButton() {
        return (ImageButton) ((Activity) context).findViewById(R.id.imagePriceList);
    }

    @Override
    protected ProgressBar getProgressBar() {
        return (ProgressBar) ((Activity) context).findViewById(R.id.progressBar);
    }

    @Override
    protected boolean databaseExists() {
        return new PriceDatabase(context).doesDatabaseExist();
    }

    @Override
    protected void performBackgroundProcessing() {
        ArrayList<PriceData> dataEntries = priceListParser.populateData(file);
        priceListParser.populateSqlDatabase(context, dataEntries);
    }
}
