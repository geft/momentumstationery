package com.lifeof843.momentumstationery.dropbox;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Systems;

import java.io.File;

public abstract class DropboxProcessor extends AsyncTask<Void[], Integer, Boolean>
        implements GenericParser.DownloadUpdater {
    protected Context context;
    protected File file;

    DropboxProcessor(Context context, File file) {
        this.context = context;
        this.file = file;
    }

    @Override
    protected void onPreExecute() {
        lockScreenOrientation();
        lockButton();
        setProgressBarVisible();
    }

    @Override
    protected void onPostExecute(Boolean isSuccess) {
        if (databaseExists()) {
            unlockButton();
        }

        unlockScreenOrientation();
        makeStatusToast(isSuccess);
        setProgressBarInvisible();
    }

    private void makeStatusToast(Boolean isSuccess) {
        if (isSuccess) {
            Systems.makeToast(context, context.getString(R.string.load_database_parse_success));
        } else {
            Systems.makeToast(context, context.getString(R.string.load_database_parse_fail));
        }
    }

    private void setProgressBarVisible() {
        getProgressBar().setVisibility(View.VISIBLE);
        getLongProgressBar().setVisibility(View.VISIBLE);
    }

    private void setProgressBarInvisible() {
        getProgressBar().setVisibility(View.INVISIBLE);
        getLongProgressBar().setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        getLongProgressBar().setProgress(values[0]);
    }

    protected void setLongProgressMax(int progressMax) {
        getLongProgressBar().setMax(progressMax);
    }

    @Override
    public void sendRowCount(int rowCount) {
        setLongProgressMax(rowCount);
    }

    public void updateProgress(int progress) {
        publishProgress(progress);
    }

    @Override
    protected Boolean doInBackground(Void[]... params) {
        boolean isSuccess = true;

        try {
            performBackgroundProcessing();
        } catch (Exception e) {
            e.printStackTrace();
            isSuccess = false;
        } finally {
            file.delete();
        }
        return isSuccess;
    }

    protected abstract void performBackgroundProcessing();

    protected abstract boolean databaseExists();

    protected ProgressBar getLongProgressBar() {
        return (ProgressBar) ((Activity) context).findViewById(R.id.longProgressBar);
    }

    private void lockButton() {
        ImageButton button = getLockableButton();
        button.setClickable(false);
        button.setAlpha(0.3f);
    }

    private void unlockButton() {
        ImageButton button = getLockableButton();
        button.setClickable(true);
        button.setAlpha(1.0f);
        ImageView view = getDisabledImage();
        view.setVisibility(View.INVISIBLE);
    }

    protected abstract ImageView getDisabledImage();

    protected abstract ImageButton getLockableButton();

    protected abstract ProgressBar getProgressBar();

    private void lockScreenOrientation() {
        int currentOrientation = context.getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void unlockScreenOrientation() {
        setOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    private void setOrientation(int orientation) {
        ((Activity) context).setRequestedOrientation(orientation);
    }
}
