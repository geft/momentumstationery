package com.lifeof843.momentumstationery.dropbox;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.vendor.VendorData;
import com.lifeof843.momentumstationery.vendor.VendorDatabase;

import java.io.File;
import java.util.ArrayList;

public class VendorListProcessor extends DropboxProcessor {

    VendorListParser vendorListParser;

    VendorListProcessor(VendorListParser vendorListParser, Context context, File file) {
        super(context, file);
        this.vendorListParser = vendorListParser;
    }

    @Override
    protected ImageView getDisabledImage() {
        return (ImageView) ((Activity) context).findViewById(R.id.imageDisabled2);
    }

    @Override
    protected ImageButton getLockableButton() {
        return (ImageButton) ((Activity) context).findViewById(R.id.imageVendorList);
    }

    @Override
    protected ProgressBar getProgressBar() {
        return (ProgressBar) ((Activity) context).findViewById(R.id.progressBar2);
    }

    @Override
    protected boolean databaseExists() {
        return new VendorDatabase(context).doesDatabaseExist();
    }

    @Override
    protected void performBackgroundProcessing() {
        ArrayList<VendorData> vendorData = vendorListParser.populateData(file);
        vendorListParser.populateSqlDatabase(context, vendorData);
    }
}
