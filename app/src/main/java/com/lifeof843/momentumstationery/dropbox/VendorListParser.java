package com.lifeof843.momentumstationery.dropbox;

import android.content.Context;

import com.lifeof843.momentumstationery.vendor.VendorData;
import com.lifeof843.momentumstationery.vendor.VendorDatabase;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.ICsvBeanReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class VendorListParser extends GenericParser {

    VendorListProcessor processor;

    @Override
    protected void beginAsyncTask(Context context, File file) {
        processor = new VendorListProcessor(this, context, file);
        processor.execute();
    }

    @Override
    protected DownloadUpdater getProcessor() {
        return processor;
    }

    @Override
    protected void beginPopulatingSqlDatabase(Context context, ArrayList dataEntries) {
        VendorDatabase vendorDatabase = new VendorDatabase(context);
        vendorDatabase.populateDatabase(dataEntries, processor);
    }

    @Override
    protected ArrayList<VendorData> parseCSV(File file) throws IOException {
        ArrayList<VendorData> vendorDataList = new ArrayList<>();
        VendorData vendorData;

        ICsvBeanReader beanReader = null;
        try {
            beanReader = getBeanReader(file);
            beanReader.getHeader(true);

            calculateRowCount(beanReader, VendorData.class);
            beanReader = restartBeanReader(beanReader, file);

            String[] headers = getProcessedHeaders();
            CellProcessor[] processors = getProcessedColumns();

            while ((vendorData = beanReader.read(VendorData.class, headers, processors)) != null) {
                vendorDataList.add(vendorData);
            }
        } finally {
            closeBeanReader(beanReader);
        }

        return vendorDataList;
    }

    @Override
    protected String[] getProcessedHeaders() {
        return new String[]{
                "name", "tag", "phone"};
    }

    @Override
    protected CellProcessor[] getProcessedColumns() {
        return new CellProcessor[]{
                new NotNull(),
                new Optional(),
                new Optional()};
    }

    @Override
    protected CellProcessor[] getNullProcessor() {
        return new CellProcessor[3];
    }
}
