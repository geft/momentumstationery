package com.lifeof843.momentumstationery.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.lifeof843.momentumstationery.R;

public class LoginAdmin extends DialogFragment {
    private static final int PASSWORD_HIDDEN = InputType.TYPE_CLASS_TEXT |
            InputType.TYPE_TEXT_VARIATION_PASSWORD;
    private static final int PASSWORD_SHOWN = InputType.TYPE_CLASS_TEXT |
            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;

    private OnResultListener listener;
    private View view;

    public LoginAdmin() {
        // Required empty public constructor
    }

    public static LoginAdmin newInstance() {
        DialogFragment fragment = new LoginAdmin();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return (LoginAdmin) fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_login_admin, null);
        this.view = view;

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(getActivity().getString(R.string.admin_prompt_title))
                .setNegativeButton(getActivity().getString(R.string.button_cancel), null)
                .create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        } else {
            View view = inflater.inflate(R.layout.fragment_login_admin, container, false);
            this.view = view;
            return view;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        initialize();
    }

    private void initialize() {
        initCheckBox();
        initLogin();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnResultListener) activity;
        } catch (ClassCastException e) {
            Log.e(this.toString(), "OnResultListener not implemented");
        }
    }

    private void initCheckBox() {
        setPasswordType(PASSWORD_HIDDEN);
        getCheckBoxPassword().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switchPasswordVisibility(isChecked);
            }
        });
    }

    private void switchPasswordVisibility(boolean isChecked) {
        if (isChecked) {
            setPasswordType(PASSWORD_SHOWN);
        } else {
            setPasswordType(PASSWORD_HIDDEN);
        }
        restoreCursorPosition();
    }

    private void restoreCursorPosition() {
        EditText editText = getEditPassword();
        editText.setSelection(editText.getText().length());
    }

    private CheckBox getCheckBoxPassword() {
        return (CheckBox) view.findViewById(R.id.checkBoxPassword);
    }

    private EditText getEditPassword() {
        return (EditText) view.findViewById(R.id.editPassword);
    }

    private Editable getEditPasswordText() {
        return getEditPassword().getText();
    }

    private String getEditPasswordString() {
        return getEditPasswordText().toString();
    }

    private void clearPassword() {
        getEditPasswordText().clear();
    }

    private void initLogin() {
        getEditPassword().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    returnPassword();
                }
                return true;
            }
        });
    }

    private void returnPassword() {
        String password = getEditPasswordString();
        cleanUp();
        listener.verifyPassword(password);
    }

    private void cleanUp() {
        clearPassword();

        if (getShowsDialog()) {
            getDialog().dismiss();
        }
    }

    private void setPasswordType(int inputType) {
        getEditPassword().setInputType(inputType);
    }

    public interface OnResultListener {
        public void verifyPassword(String password);
    }
}
