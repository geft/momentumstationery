package com.lifeof843.momentumstationery.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Dialogs;
import com.lifeof843.momentumstationery.utility.Systems;
import com.lifeof843.momentumstationery.vendor.VendorData;
import com.lifeof843.momentumstationery.vendor.VendorDatabase;

public class EditVendor extends DialogFragment {

    public static final String VENDOR_NAME = "name";

    private VendorDatabase vendorDatabase;
    private EditVendorListener listener;
    private String vendorName;

    public static Fragment newInstance(String name) {
        Bundle bundle = new Bundle();
        bundle.putString(VENDOR_NAME, name);

        Fragment fragment = new EditVendor();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            vendorName = getArguments().getString(VENDOR_NAME);
        }

        initDatabase();
        initListener();
    }

    private void initDatabase() {
        vendorDatabase = new VendorDatabase(getActivity());
    }

    private void initListener() {
        listener = (EditVendorListener) getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_edit_vendor, null, false);
        assignTextData(view);

        return getDialog(view);
    }

    private AlertDialog getDialog(final View view) {
        return Dialogs.getTwoWayDialog(view, getActivity().getString(R.string.edit_vendor_title),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        performPositiveAction(view);
                    }
                });
    }

    private void performPositiveAction(View v) {
        String name = getEditString(v, R.id.editName);
        String tag = getEditString(v, R.id.editTag);
        String phone = getEditString(v, R.id.editPhone);

        if (name.isEmpty()) {
            Systems.makeToast(getActivity(), getString(R.string.edit_vendor_blank_name));
        } else {
            VendorData data = new VendorData(name, tag, phone);
            listener.callbackResult(vendorName, data);
            dismiss();
        }
    }

    private String getEditString(View v, int resId) {
        return ((EditText) v.findViewById(resId)).getText().toString();
    }

    private void assignTextData(View view) {
        VendorData data = getVendorData();

        EditText name = (EditText) view.findViewById(R.id.editName);
        name.setText(data.getName());

        EditText tag = (EditText) view.findViewById(R.id.editTag);
        tag.setText(data.getTag());

        EditText phone = (EditText) view.findViewById(R.id.editPhone);
        phone.setText(data.getPhone());
    }

    private VendorData getVendorData() {
        return vendorDatabase.getVendorData(vendorName);
    }

    public interface EditVendorListener {
        void callbackResult(String vendorName, VendorData vendorData);
    }
}
