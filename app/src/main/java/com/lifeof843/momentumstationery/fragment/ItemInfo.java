package com.lifeof843.momentumstationery.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.pricelist.PriceData;
import com.lifeof843.momentumstationery.pricelist.PriceDatabase;
import com.lifeof843.momentumstationery.utility.Prefs;

public class ItemInfo extends Fragment {
    private static final String ITEM_NAME = "itemName";
    public static String itemName;
    private PriceDatabase priceDatabase;

    public ItemInfo() {
        // Required empty public constructor
    }

    public static ItemInfo newInstance(String itemName) {
        ItemInfo fragment = new ItemInfo();

        Bundle bundle = new Bundle();
        bundle.putString(ITEM_NAME, itemName);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            itemName = getArguments().getString(ITEM_NAME);
        }

        priceDatabase = new PriceDatabase(getActivity());
    }

    private PriceData getDataEntryFromName(String name) {
        return priceDatabase.getDataEntryFromName(name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = getInflatedLayout(container);
        assignTextViewData(view, itemName);
        return view;
    }

    protected View getInflatedLayout(ViewGroup container) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        return inflater.inflate(R.layout.price_list_item_fragment, container, false);
    }

    private void assignTextViewData(View view, String name) {
        int exchangeRate = Prefs.readExchangeRate(view.getContext());

        if (!name.isEmpty()) {
            PriceData priceData = getDataEntryFromName(name);
            setTextView(view, priceData.getName(), R.id.textName);
            setTextView(view, priceData.getUnit(), R.id.textUnit);
            setTextView(view, priceData.getCost(), R.id.textCost);
            setTextView(view, priceData.getPrice(exchangeRate), R.id.textPrice);
            setTextView(view, priceData.getPriceForUser(exchangeRate), R.id.textPriceUser);
        }
    }

    private void setTextView(View view, String value, int res) {
        TextView textView = getTextViewById(view, res);
        if (value != null) {
            textView.setText(value);
        }
    }

    private TextView getTextViewById(View view, int res) {
        return (TextView) view.findViewById(res);
    }
}
