package com.lifeof843.momentumstationery.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Dialogs;
import com.lifeof843.momentumstationery.utility.Systems;
import com.lifeof843.momentumstationery.vendor.VendorData;
import com.lifeof843.momentumstationery.vendor.VendorDatabase;

public class AddVendor extends DialogFragment {

    public static Fragment newInstance() {
        return new AddVendor();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_edit_vendor, null, false);
        return getDialog(view);
    }

    private Dialog getDialog(final View view) {
        return Dialogs.getTwoWayDialog(view, getActivity().getString(R.string.add_vendor_title),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addVendor(view);
                    }
                });
    }

    private void addVendor(View view) {
        VendorData vendorData = createVendorData(view);
        VendorDatabase vendorDatabase = new VendorDatabase(getActivity());
        boolean isSuccess = false;

        if (vendorData.getName().isEmpty()) {
            Systems.makeToast(getActivity(), getString(R.string.edit_vendor_blank_name));
        } else if (!vendorDatabase.doesEntryExist(vendorData.getName())) {
            vendorDatabase.addEntry(null, 0, vendorData);
            isSuccess = true;
        } else {
            Systems.makeToast(getActivity(), getString(R.string.vendor_list_name_exists));
        }

        callback(isSuccess);
    }

    private void callback(boolean isSuccess) {
        AddVendorListener listener = (AddVendorListener) getActivity();
        listener.addVendorResult(isSuccess);
    }

    private VendorData createVendorData(View view) {
        String name = getEditTextString(view, R.id.editName);
        String tag = getEditTextString(view, R.id.editTag);
        String phone = getEditTextString(view, R.id.editPhone);

        return new VendorData(name, tag, phone);
    }

    private String getEditTextString(View view, int resId) {
        return ((EditText) view.findViewById(resId)).getText().toString();
    }

    public interface AddVendorListener {
        public void addVendorResult(boolean isSuccess);
    }
}
