package com.lifeof843.momentumstationery.fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Prefs;

public class ExchangeRate extends DialogFragment {

    public ExchangeRate() {
        // Required empty public constructor
    }

    public static ExchangeRate newInstance() {
        return new ExchangeRate();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();

        View view = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_exchange_rate, null);
        fillData(view);

        return new AlertDialog.Builder(context)
                .setView(view)
                .setTitle(context.getString(R.string.action_exchange_rate))
                .setPositiveButton(context.getString(R.string.button_OK), null)
                .create();
    }

    private void fillData(View view) {
        int exchangeRate = Prefs.readExchangeRate(getActivity());
        String time = Prefs.readExchangeRateTime(getActivity());
        TextView textView = (TextView) view.findViewById(R.id.textExchangeRate);
        String text = String.format("%,d", exchangeRate).replace(",", ".");
        textView.setText("IDR/USD: Rp " + text + ",—\n" +
                "Last update: " + time + "\n" +
                "Source: Central European Bank");
    }
}
