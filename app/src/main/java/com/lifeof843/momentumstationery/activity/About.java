package com.lifeof843.momentumstationery.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import com.lifeof843.momentumstationery.BuildConfig;
import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Systems;


public class About extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        initVersion();
    }

    private void initVersion() {
        TextView textView = (TextView) findViewById(R.id.textVersion);
        textView.setText("Momentum Stationery v" + BuildConfig.VERSION_NAME);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Systems.fadeAnimation(this);
    }
}
