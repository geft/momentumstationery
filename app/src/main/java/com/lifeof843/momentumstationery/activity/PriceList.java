package com.lifeof843.momentumstationery.activity;

import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.fragment.ItemInfo;
import com.lifeof843.momentumstationery.pricelist.PriceDatabase;
import com.lifeof843.momentumstationery.pricelist.PriceListAdapter;
import com.lifeof843.momentumstationery.pricelist.PriceListAdapterLandscape;

import java.util.ArrayList;

public class PriceList extends SearchableActivity {

    private PriceDatabase priceDatabase;

    @Override
    protected void initState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            replaceContainerFragment();
        }
    }

    @Override
    protected void initDatabase() {
        priceDatabase = new PriceDatabase(this);
    }

    @Override
    protected void initList(Cursor cursor) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerList);
        recyclerView.setLayoutManager(getLayoutManager());
        recyclerView.setItemAnimator(getItemAnimator());

        if (priceDatabase.doesDatabaseExist()) {
            recyclerView.setAdapter(getRecyclerAdapter(cursor));
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_price_list;
    }

    @Override
    protected int getMenuRes() {
        return R.menu.menu_price_list;
    }

    @Override
    protected void setQueryTextChangeListener(String query) {
        initList(priceDatabase.getCursor(query));
    }

    private PriceListAdapter getRecyclerAdapter(Cursor cursor) {
        ArrayList<String> nameArray = priceDatabase.getNameArray(cursor);

        if (isPortrait()) {
            return new PriceListAdapter(this, nameArray);
        } else {
            return new PriceListAdapterLandscape(this, nameArray);
        }
    }

    private RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    private RecyclerView.ItemAnimator getItemAnimator() {
        return new DefaultItemAnimator();
    }

    private void replaceContainerFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, ItemInfo.newInstance(ItemInfo.itemName));
        ft.commit();
    }

    private int getOrientation() {
        return getResources().getConfiguration().orientation;
    }

    private boolean isPortrait() {
        return getOrientation() == Configuration.ORIENTATION_PORTRAIT;
    }

    @Override
    protected void onDestroy() {
        closeDatabase();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        closeDatabase();
        super.onBackPressed();
    }

    private void closeDatabase() {
        if (priceDatabase != null) {
            if (!priceDatabase.isLocked()) {
                priceDatabase.close();
            }
        }
    }
}
