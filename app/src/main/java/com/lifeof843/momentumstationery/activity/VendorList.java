package com.lifeof843.momentumstationery.activity;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.ListView;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.fragment.AddVendor;
import com.lifeof843.momentumstationery.fragment.EditVendor;
import com.lifeof843.momentumstationery.utility.Dialogs;
import com.lifeof843.momentumstationery.utility.Systems;
import com.lifeof843.momentumstationery.vendor.VendorData;
import com.lifeof843.momentumstationery.vendor.VendorDatabase;
import com.lifeof843.momentumstationery.vendor.VendorExporter;
import com.lifeof843.momentumstationery.vendor.VendorListAdapter;

public class VendorList extends SearchableActivity implements
        EditVendor.EditVendorListener, AddVendor.AddVendorListener {
    VendorDatabase vendorDatabase;

    @Override
    protected void initState(Bundle savedInstanceState) {
    }

    @Override
    protected void initDatabase() {
        vendorDatabase = new VendorDatabase(this);
    }

    @Override
    protected void initList(Cursor cursor) {
        if (cursor == null) {
            cursor = vendorDatabase.getCursor();
        }

        ListView listVendor = getListView();
        listVendor.setVerticalScrollBarEnabled(false);
        listVendor.setAdapter(getAdapter(cursor));
    }

    @Override
    protected void setQueryTextChangeListener(String query) {
        initList(vendorDatabase.getCursor(query));
    }

    private ListView getListView() {
        return (ListView) findViewById(R.id.listVendor);
    }

    private VendorListAdapter getAdapter(Cursor cursor) {
        String[] from = new String[]{VendorDatabase.COL_NAME, VendorDatabase.COL_TAG, VendorDatabase.COL_PHONE};
        int[] to = new int[]{R.id.textName, R.id.textTag, R.id.textPhone};

        return new VendorListAdapter(this, cursor, from, to);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_vendor_list;
    }

    @Override
    protected int getMenuRes() {
        return R.menu.menu_vendor_list;
    }

    @Override
    public void callbackResult(String vendorName, VendorData vendorData) {
        boolean isSameName = vendorName.equals(vendorData.getName());
        if (isSameName) {
            updateVendor(vendorName, vendorData);
        } else if (!vendorDatabase.doesEntryExist(vendorData.getName())) {
            updateVendor(vendorName, vendorData);
        } else {
            Systems.makeToast(this, getString(R.string.vendor_list_name_exists));
        }
    }

    private void updateVendor(String vendorName, VendorData vendorData) {
        vendorDatabase.updateEntry(vendorName, vendorData);
        refreshList();
    }

    private void refreshList() {
        initList(vendorDatabase.getCursor());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add_vendor) {
            createAddVendorDialog();
        } else if (item.getItemId() == R.id.action_export) {
            createExportDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    private void createExportDialog() {
        Dialogs.getWarningPrompt(this, getString(R.string.vendor_exporter_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        VendorExporter vendorExporter = new VendorExporter(VendorList.this);
                        vendorExporter.execute();
                        dialog.dismiss();
                    }
                }).show();
    }

    private void createAddVendorDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(AddVendor.newInstance(), "dialog");
        ft.commit();
    }

    @Override
    public void addVendorResult(boolean isSuccess) {
        if (isSuccess) {
            refreshList();
        }
    }

    @Override
    protected void onDestroy() {
        closeDatabase();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        closeDatabase();
        super.onBackPressed();
    }

    private void closeDatabase() {
        if (vendorDatabase != null) {
            if (!vendorDatabase.isLocked()) {
                vendorDatabase.close();
            }
        }
    }
}
