package com.lifeof843.momentumstationery.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageButton;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.dropbox.DropboxLoader;
import com.lifeof843.momentumstationery.utility.PasswordManager;
import com.lifeof843.momentumstationery.utility.Systems;


public class Settings extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initDropBoxKeyChange();
        initPasswordChange();
    }

    private void initDropBoxKeyChange() {
        ImageButton buttonKey = (ImageButton) findViewById(R.id.buttonDropboxKey);
        buttonKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DropboxLoader dropboxLoader = new DropboxLoader(v.getContext());
                dropboxLoader.changeKey(v.getContext());
            }
        });
    }

    private void initPasswordChange() {
        ImageButton buttonPassword = (ImageButton) findViewById(R.id.buttonPassword);
        buttonPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordManager passwordManager = new PasswordManager();
                passwordManager.createPasswordChangeDialog(v.getContext());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Systems.fadeAnimation(this);
    }
}
