package com.lifeof843.momentumstationery.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.dropbox.DropboxLoader;
import com.lifeof843.momentumstationery.fragment.ExchangeRate;
import com.lifeof843.momentumstationery.fragment.LoginAdmin;
import com.lifeof843.momentumstationery.pricelist.PriceDatabase;
import com.lifeof843.momentumstationery.utility.Dialogs;
import com.lifeof843.momentumstationery.utility.MenuItemBuilder;
import com.lifeof843.momentumstationery.utility.PasswordManager;
import com.lifeof843.momentumstationery.utility.Prefs;
import com.lifeof843.momentumstationery.utility.Systems;
import com.lifeof843.momentumstationery.vendor.VendorDatabase;


public class MainMenu extends ActionBarActivity implements LoginAdmin.OnResultListener {

    private DropboxLoader dropboxLoader;
    private PasswordManager passwordManager;
    private MenuItemBuilder menuItemBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        initManagers();
    }

    private void initManagers() {
        dropboxLoader = new DropboxLoader(this);
        passwordManager = new PasswordManager();
        menuItemBuilder = new MenuItemBuilder(this, dropboxLoader);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (dropboxLoader != null) {
            dropboxLoader.handleDropBoxResult(requestCode, resultCode, data, this);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (menuItemBuilder != null) {
            menuItemBuilder.animateAllViewsEnter();
            initPriceListButton();
            initVendorListButton();
        }
    }

    private void initPriceListButton() {
        PriceDatabase priceDatabase = new PriceDatabase(this);
        if (!priceDatabase.doesDatabaseExist()) {
            disableButton(R.id.imagePriceList, R.id.imageDisabled);
        }
    }

    private void initVendorListButton() {
        VendorDatabase vendorDatabase = new VendorDatabase(this);
        if (!vendorDatabase.doesDatabaseExist()) {
            disableButton(R.id.imageVendorList, R.id.imageDisabled2);
        }
    }

    private void disableButton(int imageRes, int disableRes) {
        ImageButton button = (ImageButton) findViewById(imageRes);
        button.setClickable(false);
        button.setAlpha(0.3f);

        ImageView view = (ImageView) findViewById(disableRes);
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_settings:
                createAdminPromptDialog();
                break;
            case R.id.action_exchange_rate:
                createExchangeRateDialog();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void createExchangeRateDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(ExchangeRate.newInstance(), "dialog");
        ft.commit();
    }

    private void createAdminPromptDialog() {
        passwordManager.createAdminPromptDialog(getSupportFragmentManager());
    }

    @Override
    public void verifyPassword(String password) {
        if (password.equals(Prefs.readPassword(this))) {
            startActivity(new Intent(this, Settings.class));
            Systems.fadeAnimation(this);
        } else {
            Systems.makeToast(this, getString(R.string.admin_prompt_invalid));
        }
    }

    @Override
    public void onBackPressed() {
        initBackDialog();
    }

    private void initBackDialog() {
        Dialogs.getWarningPrompt(
                this, getString(R.string.main_menu_back_prompt), getBackDialogListener()).show();
    }

    private DialogInterface.OnClickListener getBackDialogListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(MainMenu.this, Login.class));
                Systems.fadeAnimation(MainMenu.this);
            }
        };
    }
}
