package com.lifeof843.momentumstationery.activity;

import android.app.SearchManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Systems;

public abstract class SearchableActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRes());

        initState(savedInstanceState);
        initDatabase();
        initList(null);
    }

    protected abstract void initState(Bundle savedInstanceState);

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(getMenuRes(), menu);
        initSearch(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Systems.fadeAnimation(this);
    }

    protected abstract void initList(Cursor cursor);

    protected abstract void initDatabase();

    protected abstract int getLayoutRes();

    protected abstract int getMenuRes();

    private void initSearch(Menu menu) {
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        setQueryListener(searchView);
        setQueryClearListener(searchView);
    }

    private void setQueryClearListener(SearchView searchView) {
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                initList(null);
                return false;
            }
        });
    }

    private void setQueryListener(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setQueryTextChangeListener(newText);
                return false;
            }
        });
    }

    protected abstract void setQueryTextChangeListener(String query);
}
