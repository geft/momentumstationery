package com.lifeof843.momentumstationery.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.exchange_rate.ExchangeRateProcessor;
import com.lifeof843.momentumstationery.exchange_rate.IScraper;
import com.lifeof843.momentumstationery.fragment.LoginAdmin;
import com.lifeof843.momentumstationery.utility.Prefs;
import com.lifeof843.momentumstationery.utility.Systems;

public class Login extends ActionBarActivity implements LoginAdmin.OnResultListener, IScraper {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initExchangeRate(savedInstanceState);
        initFragment();
    }

    private void initExchangeRate(Bundle savedInstanceState) {
        if (savedInstanceState == null && Systems.isNetworkActive(this)) {
            new ExchangeRateProcessor(this);
        }
    }

    private void initFragment() {
        if (getLoginFragment() == null) {
            createFragment();
        }
    }

    private Fragment getLoginFragment() {
        return getSupportFragmentManager().findFragmentByTag("login_frame");
    }

    private void createFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, LoginAdmin.newInstance(), "login_frame");
        ft.commit();
    }

    @Override
    public void verifyPassword(String password) {
        if (password.equals(Prefs.readPassword(this))) {
            startActivity(new Intent(Login.this, MainMenu.class));
            Systems.fadeAnimation(this);
        } else {
            Systems.makeToast(this, getString(R.string.login_invalid_password));
        }
    }

    @Override
    public void onScrapeDone(int exchangeRate, String time) {
        if (exchangeRate != 0 && time != null) {
            Prefs.writeExchangeRate(this, exchangeRate);
            Prefs.writeExchangeRateTime(this, time);
        }
    }
}
