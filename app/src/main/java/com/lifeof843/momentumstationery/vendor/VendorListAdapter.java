package com.lifeof843.momentumstationery.vendor;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.fragment.EditVendor;
import com.lifeof843.momentumstationery.utility.Dialogs;

public class VendorListAdapter extends SimpleCursorAdapter {

    private Context context;
    private boolean isPhone;

    public VendorListAdapter(Context context, Cursor c, String[] from, int[] to) {
        super(context, R.layout.vendor_list_list_item, c, from, to, FLAG_REGISTER_CONTENT_OBSERVER);

        this.context = context;
        isPhone = hasTelephony(context);
    }

    @Override
    public void bindView(@NonNull View view, Context context, @NonNull Cursor cursor) {
        super.bindView(view, context, cursor);

        final ViewHolder holder = (ViewHolder) view.getTag();

        setupCallButton(holder);
        setupEditButton(holder);
        setupDeleteButton(holder);
    }

    private void setupDeleteButton(ViewHolder holder) {
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDeleteVendorDialog(getNameFromView(v));
            }
        });
    }

    private void createDeleteVendorDialog(final String vendorName) {
        Dialogs.getWarningPrompt(context, context.getString(R.string.vendor_list_delete),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteVendor(dialog, vendorName);
                    }
                }).show();
    }

    private void deleteVendor(DialogInterface dialog, String vendorName) {
        VendorDatabase vendorDatabase = new VendorDatabase(context);
        vendorDatabase.deleteEntry(vendorName);
        swapCursor(vendorDatabase.getCursor());
        dialog.dismiss();
    }

    private void setupEditButton(ViewHolder holder) {
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEditVendorDialog(getNameFromView(v));
            }
        });
    }

    private String getNameFromView(View v) {
        View view = (View) v.getParent();
        return ((TextView) view.findViewById(R.id.textName)).getText().toString();
    }

    private void createEditVendorDialog(String name) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(EditVendor.newInstance(name), "dialog");
        ft.commit();
    }

    private FragmentManager getSupportFragmentManager() {
        return ((FragmentActivity) context).getSupportFragmentManager();
    }

    private void setupCallButton(final ViewHolder holder) {
        if (isPhone) {
            holder.call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startDialler(v, holder);
                }
            });
        } else {
            holder.call.setVisibility(View.GONE);
        }
    }

    private boolean hasTelephony(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    private void startDialler(View v, ViewHolder holder) {
        Uri phoneNumber = Uri.parse("tel:" + holder.phone.getText().toString());
        v.getContext().startActivity(new Intent(Intent.ACTION_DIAL, phoneNumber));
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.vendor_list_list_item, parent, false);
        view.setTag(getViewHolder(view));

        return view;
    }

    private ViewHolder getViewHolder(View view) {
        ViewHolder holder = new ViewHolder();
        holder.call = (ImageButton) view.findViewById(R.id.buttonCall);
        holder.edit = (ImageButton) view.findViewById(R.id.buttonEdit);
        holder.delete = (ImageButton) view.findViewById(R.id.buttonDelete);
        holder.phone = (TextView) view.findViewById(R.id.textPhone);

        return holder;
    }

    private class ViewHolder {
        ImageButton call;
        ImageButton edit;
        ImageButton delete;
        TextView phone;
    }
}
