package com.lifeof843.momentumstationery.vendor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.lifeof843.momentumstationery.dropbox.GenericParser;
import com.lifeof843.momentumstationery.utility.AbstractDatabase;

import java.util.ArrayList;

public class VendorDatabase extends AbstractDatabase {

    public static final String COL_TAG = "tag";
    public static final String COL_PHONE = "phone";

    private static final String TABLE_NAME = "vendor_list";
    private static final String DATABASE_NAME = "vendor.db";

    public VendorDatabase(Context context) {
        super(context, DATABASE_NAME);
    }

    @Override
    protected String getDbQueryCreate() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_NAME + " TEXT UNIQUE NOT NULL COLLATE NOCASE, " +
                COL_TAG + " TEXT COLLATE NOCASE, " +
                COL_PHONE + " TEXT)";
    }

    @Override
    protected String getDbTableName() {
        return TABLE_NAME;
    }

    public ArrayList<VendorData> getVendorArray() {
        ArrayList<VendorData> list = new ArrayList<>();

        Cursor cursor = getAllCursor();
        while (cursor.moveToNext()) {
            String name = getColumnString(cursor, COL_NAME);
            String tag = getColumnString(cursor, COL_TAG);
            String phone = getColumnString(cursor, COL_PHONE);
            list.add(new VendorData(name, tag, phone));
        }
        cursor.close();

        return list;
    }

    private Cursor getAllCursor() {
        return database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    public void populateDatabase(
            ArrayList<VendorData> vendorDataList, GenericParser.DownloadUpdater updater) {
        resetDatabase();

        insertEntry(vendorDataList, updater, 0);
    }

    @Override
    public void addEntry(ContentValues contentValues, int exchangeRate, Object object) {
        if (contentValues == null) {
            contentValues = new ContentValues();
        }

        VendorData data = (VendorData) object;
        contentValues.put(COL_NAME, data.getName());
        contentValues.put(COL_TAG, data.getTag());
        contentValues.put(COL_PHONE, data.getPhone());
        database.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getCursor(String query) {
        return database.rawQuery(
                "SELECT * FROM " + TABLE_NAME +
                        " WHERE " + COL_NAME + " LIKE '%" + query + "%'" +
                        " OR " + COL_TAG + " LIKE '%" + query + "%'" +
                        " ORDER BY " + COL_NAME + " ASC", null
        );
    }

    public VendorData getVendorData(String vendorName) {
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME +
                " WHERE " + COL_NAME + "=?", new String[]{vendorName});

        String tag;
        String phone;
        VendorData data = null;

        if (cursor.moveToFirst()) {
            tag = cursor.getString(cursor.getColumnIndex(COL_TAG));
            phone = cursor.getString(cursor.getColumnIndex(COL_PHONE));
            data = new VendorData(vendorName, tag, phone);
        }
        cursor.close();

        return data;
    }

    public void updateEntry(String vendorName, VendorData vendorData) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME, vendorData.getName());
        contentValues.put(COL_TAG, vendorData.getTag());
        contentValues.put(COL_PHONE, vendorData.getPhone());

        database.update(TABLE_NAME, contentValues, COL_NAME + "=?", new String[]{vendorName});
    }
}
