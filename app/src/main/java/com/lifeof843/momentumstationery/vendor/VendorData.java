package com.lifeof843.momentumstationery.vendor;

public class VendorData {

    private String name;
    private String tag;
    private String phone;

    public VendorData() {
        // required for CSV parser
    }

    public VendorData(String name, String tag, String phone) {
        this.name = name;
        this.tag = tag;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        phone = phone.replace("-", "");

        if (phone.startsWith("8") && phone.length() > 10) {
            phone = "0" + phone;
        } else if (!phone.isEmpty() && !phone.startsWith("021")) {
            phone = "021" + phone;
        }

        this.phone = phone;
    }
}
