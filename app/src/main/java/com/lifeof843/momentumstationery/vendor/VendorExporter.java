package com.lifeof843.momentumstationery.vendor;

import android.content.Context;
import android.os.AsyncTask;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.utility.Systems;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class VendorExporter extends AsyncTask<Void, Void, Boolean> {

    public static final String FILE_NAME = "/vendor_list.csv";
    Context context;

    public VendorExporter(Context context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        boolean isSuccess = false;

        VendorDatabase vendorDatabase = new VendorDatabase(context);
        ArrayList<VendorData> arrayList = vendorDatabase.getVendorArray();

        try {
            if (parseToCSV(arrayList)) {
                isSuccess = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return isSuccess;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean) {
            makeSuccessToast();
        } else {
            makeFailToast();
        }
    }

    private boolean parseToCSV(ArrayList<VendorData> arrayList) throws IOException {
        ICsvBeanWriter beanWriter = null;
        try {
            String filePath = getFilePath();
            beanWriter = new CsvBeanWriter(new FileWriter(filePath),
                    CsvPreference.STANDARD_PREFERENCE);
            String[] headers = getHeaders();
            CellProcessor[] processors = getProcessors();
            beanWriter.writeHeader(headers);

            for (VendorData entry : arrayList) {
                beanWriter.write(entry, headers, processors);
            }

        } finally {
            closeBeanWriter(beanWriter);
        }

        return true;
    }

    private String getFilePath() {
        return context.getExternalFilesDir(null) + FILE_NAME;
    }

    private void closeBeanWriter(ICsvBeanWriter beanWriter) throws IOException {
        if (beanWriter != null) {
            beanWriter.close();
        }
    }

    private CellProcessor[] getProcessors() {
        return new CellProcessor[]{new NotNull(), new Optional(), new Optional()};
    }

    private String[] getHeaders() {
        return new String[]{"name", "tag", "phone"};
    }

    private void makeSuccessToast() {
        Systems.makeLongToast(context,
                context.getString(R.string.vendor_exporter_success) + " " + getFilePath());
    }

    private void makeFailToast() {
        Systems.makeToast(context, context.getString(R.string.vendor_exporter_fail));
    }
}
