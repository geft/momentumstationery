package com.lifeof843.momentumstationery.pricelist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.lifeof843.momentumstationery.dropbox.GenericParser;
import com.lifeof843.momentumstationery.utility.AbstractDatabase;

import java.util.ArrayList;

public class PriceDatabase extends AbstractDatabase {

    public static final String COL_COST = "cost";
    public static final String COL_PRICE = "price";
    public static final String COL_PRICE_USER = "priceUser";
    public static final String COL_UNIT = "unit";

    private static final String DATABASE_NAME = "data.db";
    private static final String TABLE_NAME = "price_list";

    public PriceDatabase(Context context) {
        super(context, DATABASE_NAME);
    }

    @Override
    protected String getDbTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getDbQueryCreate() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_NAME + " TEXT UNIQUE NOT NULL COLLATE NOCASE, " +
                COL_UNIT + " TEXT, " +
                COL_COST + " TEXT, " +
                COL_PRICE + " TEXT, " +
                COL_PRICE_USER + " TEXT)";
    }

    public void populateDatabase(ArrayList<PriceData> dataEntries, int exchangeRate,
                                 GenericParser.DownloadUpdater updater) {
        resetDatabase();
        insertEntry(dataEntries, updater, exchangeRate);
    }

    @Override
    public void addEntry(ContentValues contentValues, int exchangeRate, Object object) {
        if (contentValues == null) {
            contentValues = new ContentValues();
        }

        PriceData data = (PriceData) object;
        contentValues.put(COL_NAME, data.getName());
        contentValues.put(COL_UNIT, data.getUnit());
        contentValues.put(COL_COST, data.getCost());
        contentValues.put(COL_PRICE, data.getPrice(exchangeRate));
        contentValues.put(COL_PRICE_USER, data.getPriceForUser(exchangeRate));
        database.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getCursor(String query) {
        return database.rawQuery(
                "SELECT * FROM " + TABLE_NAME +
                        " WHERE " + COL_NAME + " LIKE '%" + query + "%'" +
                        " ORDER BY " + COL_NAME + " ASC", null
        );
    }

    public ArrayList<String> getNameArray(Cursor cursor) {
        ArrayList<String> list = new ArrayList<>();

        if (cursor == null) {
            cursor = getCursor();
        }

        while (cursor.moveToNext()) {
            list.add(cursor.getString(cursor.getColumnIndex(COL_NAME)));
        }
        cursor.close();

        return list;
    }

    public PriceData getDataEntryFromName(String name) {
        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + TABLE_NAME + " WHERE " + COL_NAME + "=?", new String[]{name});

        PriceData priceData = new PriceData();
        priceData.setName(name);

        if (cursor.moveToFirst()) {
            priceData.setUnit(cursor.getString(cursor.getColumnIndex(COL_UNIT)));
            priceData.setCost(cursor.getString(cursor.getColumnIndex(COL_COST)));
        }
        cursor.close();

        return priceData;
    }
}
