package com.lifeof843.momentumstationery.pricelist;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lifeof843.momentumstationery.R;
import com.lifeof843.momentumstationery.fragment.ItemInfo;

import java.util.ArrayList;

public class PriceListAdapter extends RecyclerView.Adapter<PriceListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<String> nameArray;

    public PriceListAdapter(Context context, ArrayList<String> nameArray) {
        this.context = context;
        this.nameArray = nameArray;

        initContainer();
    }

    private void initContainer() {
        getFragmentContainer().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateContainerExit(v);
            }
        });
    }

    private FragmentManager getFragmentManager() {
        return ((FragmentActivity) context).getSupportFragmentManager();
    }

    private FrameLayout getFragmentContainer() {
        return (FrameLayout) ((Activity) context).findViewById(R.id.container);
    }

    protected void animateContainerExit(View v) {
        v.animate()
                .translationY(v.getHeight())
                .alpha(0.0f);
    }

    protected void animateContainerEnter(View v) {
        v.animate()
                .translationY(0)
                .alpha(1.0f);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.price_list_list_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String itemName = nameArray.get(position);
        holder.name.setText(itemName);
        ((View) holder.name.getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateViewOnClick(v);
                animateContainerEnter(getFragmentContainer());
                displayItemInfo(itemName);
                hideKeyboard(v);
            }
        });
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputManager = (InputMethodManager) view.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void animateViewOnClick(final View view) {
        final float scaleFactor = 0.1f;
        view.animate()
                .scaleXBy(-scaleFactor)
                .scaleYBy(-scaleFactor)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.animate()
                                .scaleXBy(scaleFactor)
                                .scaleYBy(scaleFactor)
                                .setListener(null);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
    }

    @Override
    public int getItemCount() {
        return nameArray.size();
    }

    private void displayItemInfo(String itemName) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);
        ft.replace(R.id.container, createFragment(itemName));
        ft.commit();
    }

    protected ItemInfo createFragment(String itemName) {
        return ItemInfo.newInstance(itemName);
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.textName);
        }
    }
}
