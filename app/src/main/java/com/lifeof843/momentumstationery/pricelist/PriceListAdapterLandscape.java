package com.lifeof843.momentumstationery.pricelist;

import android.content.Context;
import android.view.View;

import java.util.ArrayList;

public class PriceListAdapterLandscape extends PriceListAdapter {

    public PriceListAdapterLandscape(Context context, ArrayList<String> nameArray) {
        super(context, nameArray);
    }

    @Override
    protected void animateContainerExit(View v) {
    }

    @Override
    protected void animateContainerEnter(View v) {
    }
}
