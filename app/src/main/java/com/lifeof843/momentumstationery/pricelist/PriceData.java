package com.lifeof843.momentumstationery.pricelist;

public class PriceData {
    private static final double PRICE_MULTIPLIER = 1.04;
    private static final double PRICE_USER_MULTIPLIER = 1.08;
    private String name;
    private String cost;
    private String unit;
    private String vendor;
    private int exchangeRate = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        if (isRupiah(cost) || isDollar(cost)) {
            this.cost = cost;
        } else {

            if (cost.endsWith(".00")) {
                cost = cost.replace(".00", "");
            } else if (cost.endsWith(",00")) {
                cost = cost.replace(",00", "");
            }

            if (cost.length() < 3) {
                writeDollarCost(cost);
            } else {
                writeRupiahCost(cost);
            }
        }
    }

    private void writeDollarCost(String cost) {
        this.cost = "$" + cost;
    }

    private void writeRupiahCost(String cost) {
        this.cost = getFormattedCurrency(
                Long.toString(roundUpNearest500((long) Double.parseDouble(cost))));
    }

    private boolean isRupiah(String cost) {
        return cost.contains("Rp");
    }

    public String getPrice(int rate) {
        exchangeRate = rate;
        if (isDollar(cost)) {
            return getMultipliedDollarCost(PRICE_MULTIPLIER);
        } else {
            return getMultipliedRupiahCost(PRICE_MULTIPLIER);
        }
    }

    private boolean isDollar(String cost) {
        return cost.contains("$");
    }

    public String getPriceForUser(int rate) {
        exchangeRate = rate;
        if (isDollar(cost)) {
            return getMultipliedDollarCost(PRICE_USER_MULTIPLIER);
        } else {
            return getMultipliedRupiahCost(PRICE_USER_MULTIPLIER);
        }
    }

    private String getMultipliedDollarCost(double multiplier) {
        if (exchangeRate == 0) {
            exchangeRate = 13200;
        }

        long longCost = Long.parseLong(cost.replace("$", ""));
        long product = Math.round(multiplier * longCost * exchangeRate);
        String stringProduct = Long.toString(roundUpNearest500(product));
        return getFormattedCurrency(stringProduct);
    }

    private String getMultipliedRupiahCost(double multiplier) {
        long longCost = getRemovedFormatting(cost);
        long product = Math.round(multiplier * longCost);
        String stringProduct = Long.toString(roundUpNearest500(product));
        return getFormattedCurrency(stringProduct);
    }

    private long getRemovedFormatting(String string) {
        string = string.replace("Rp ", "").replace(",-", "").replace(".", "");
        return Long.parseLong(string);
    }

    private long roundUpNearest500(long number) {
        return ((number + 499) / 500) * 500;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    private String getFormattedCurrency(String currency) {
        currency = currency.replace(".00", "");
        currency = String.format("%,d", Long.parseLong(currency));
        currency = currency.replace(",", ".");

        return "Rp " + currency + ",-";
    }
}
